import { PartialType } from "@nestjs/mapped-types";
import { IsString } from "class-validator";

export class CreateCarDTO {
  @IsString()
  name: string;

  @IsString()
  model: string;

  @IsString()
  color: string;

  @IsString()
  description: string;
}
export class UpdateCarDTO extends PartialType(CreateCarDTO){
}
