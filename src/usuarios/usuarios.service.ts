import { Injectable, NotFoundException } from '@nestjs/common';

import { InjectRepository } from '@nestjs/typeorm';
import { Repository, MoreThanOrEqual, LessThanOrEqual } from 'typeorm';
import { CreateUsuarioDto } from './dto/create-usuario.dto';
import { UpdateUsuarioDto } from './dto/update-usuario.dto';
import { Usuario } from './entities/usuario.entity';

enum EdadParam {
  MAJOR = 'major_edad',
  MENOR = 'menor_edad',
}

@Injectable()
export class UsuariosService {
  constructor(
    @InjectRepository(Usuario)
    private readonly usuarioRepository: Repository<Usuario>,
  ) {}

  create(createUsuarioDto: CreateUsuarioDto) {
    const usuario = this.usuarioRepository.create(createUsuarioDto);
    return this.usuarioRepository.save(usuario);
  }

  private findPropertEdad(queryEdad) {
    if (EdadParam.MAJOR in queryEdad) {
      return MoreThanOrEqual(queryEdad.major_edad || 18);
    } else if (EdadParam.MENOR in queryEdad) {
      return LessThanOrEqual(queryEdad.menor_edad || 18);
    }
    return null;
  }

  filterByMayorEdad(queryEdad: { major_edad?: number; menor_edad?: number }) {
    const filterByEdad = this.findPropertEdad(queryEdad);
    if (filterByEdad) {
      return this.usuarioRepository.findBy({ edad: filterByEdad });
    }
    throw new NotFoundException(`Consulta de edad invalida`);
  }

  findAll() {
    return this.usuarioRepository.find();
  }

  async findOne(id: string) {
    const usuario = await this.usuarioRepository.findOneBy({ id });
    if (!usuario) {
      throw new NotFoundException(`Code #${id} not found`);
    }
    return usuario;
  }

  async update(id: string, updateUsuarioDto: UpdateUsuarioDto) {
    const usuario = await this.usuarioRepository.preload({
      id,
      ...updateUsuarioDto,
    });
    if (!usuario) {
      throw new NotFoundException(`Usuario #${id} not found`);
    }
    return this.usuarioRepository.save(usuario);
  }

  async remove(id: string) {
    const usuario = await this.findOne(id);
    return this.usuarioRepository.remove(usuario);
  }
}
