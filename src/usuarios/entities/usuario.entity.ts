import { Car } from 'src/entity/Car';
import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';

@Entity('usuarios')
export class Usuario {
  @PrimaryGeneratedColumn('increment')
  id: string;

  @Column()
  nombre: string;

  @Column()
  primer_apellido: string;

  @Column()
  segundo_apellido: string;

  @Column()
  edad: number;

  @Column()
  documento_identidad: number;
}
