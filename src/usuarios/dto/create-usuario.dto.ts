import { IsNumber, IsString } from "class-validator";

export class CreateUsuarioDto {
  @IsString()
  nombre: string;

  @IsString()
  primer_apellido: string;

  @IsString()
  segundo_apellido: string;

  @IsNumber()
  edad: number;

  @IsNumber()
  documento_identidad: number;
}
