import { Module } from '@nestjs/common';
import { CarModule } from './car/car.module';
import { dbConfig } from './dbConfig';
import { UsuariosModule } from './usuarios/usuarios.module';
@Module({
  imports: [dbConfig, CarModule, UsuariosModule],
})
export class AppModule {}
